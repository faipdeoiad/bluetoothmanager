//
//  BluetoothManager.swift
//  SCAYA
//
//  Created by Žiga Porenta on 27/09/2017.
//  Copyright © 2017 umwerk. All rights reserved.
//

import UIKit
import CoreBluetooth

protocol WaterBottleDelegate {
    func bottleConnected()
    func didReadColor(color: UIColor)
    func gotNewDataSets()
}

class BluetoothManager: NSObject{
    
    static let shared = BluetoothManager()
    
    var centralManager: CBCentralManager!
    var waterBottle: CBPeripheral?
    
    // NOT SCAYA
    var ledCharacteristic:CBCharacteristic?
    var sliderCharacteristic:CBCharacteristic?
    ///
    
    var generalCharacteristic: CBCharacteristic?
    
    var timeCharacteristic: CBCharacteristic?
    var reminderCharacteristic: CBCharacteristic?
    var recomendedCharacteristic: CBCharacteristic?
    var consumedCharacteristic: CBCharacteristic?
    var intervalCharacteristic: CBCharacteristic?
    var progressCharacteristic: CBCharacteristic?
    
    var clearAllCharacteristic: CBCharacteristic?
    var factoryResetCharacteristic: CBCharacteristic?
    
    //DATA Sets
    var dataSetsCharacteristic: CBCharacteristic?
    var dataSetIndexCharacteristic: CBCharacteristic?
    var valuesCharacteristic: CBCharacteristic?
    var numberOfDataSets = 0
    ////
    
    //Firmware update
    
    var fwDataCharacteristic: CBCharacteristic?
    var crcCharacteristic: CBCharacteristic?
    var updateCMDCharacteristic: CBCharacteristic?
    
    var keepScaning = true
    
    var delegate: WaterBottleDelegate!
    
    
    override init() {
        super.init()
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    

    func scanForDevices(){
        print("Scan")
        
        if centralManager.isScanning{
            print("Manager is already scanning")
            return
        }
    
        //TODO: when actual bottle arrives scan for actual services
       centralManager.scanForPeripherals(withServices: [ScayaBottle.Services.iconServiceUUID.cbUUID(), ScayaBottle.Services.ledServiceUUID.cbUUID(), ScayaBottle.Services.slideServiceUUID.cbUUID()], options: nil)
        
    }
    
    func disconnect(){
        
        guard let bottle = waterBottle else {
            return
        }
        
        if bottle.state != .connected{
            waterBottle = nil
            return
        }
        
        guard let services = bottle.services else{
            centralManager.cancelPeripheralConnection(bottle)
            return
        }
        
        services.forEach{ service in
            if let characteristics = service.characteristics {
                characteristics.forEach{
                    if $0.uuid == ScayaBottle.Services.ledServiceUUID.cbUUID(){
                        bottle.setNotifyValue(false, for: $0)
                    }
                }
            }
        }
        
        centralManager.cancelPeripheralConnection(bottle)
        
    }
}

extension BluetoothManager: CBCentralManagerDelegate, CBPeripheralDelegate{

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
       print("Central Manager State Updated: \(central.state)")
        
        switch central.state {
        case .poweredOn:
            print("POWERED ON")
            scanForDevices()
        case .poweredOff:
            print("Bluetooth on this device is currently powered off.")
            waterBottle = nil
        case .unsupported:
            print("This device does not support Bluetooth Low Energy.")
        case .unauthorized:
            print("This app is not authorized to use Bluetooth Low Energy.")
        case .resetting:
            print("The BLE Manager is resetting; a state update is pending.")
        case .unknown:
            print("The state of the BLE Manager is unknown.")
        }
    }

    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Failed to connect")
        print(error?.localizedDescription ?? "no desc")
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        print("FOUND SOME PERIFERAL")
        
        guard let name = advertisementData[CBAdvertisementDataLocalNameKey] as? String else {return}
        if name == ScayaBottle.name.rawValue{
            
            print("Found icon board")
            keepScaning = false
            
            waterBottle = peripheral
            waterBottle?.delegate = self
            centralManager.connect(waterBottle!, options: nil)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        print("DID CONNECT")
 
        centralManager.stopScan()
        print("Scanning Stopped!")
        
        peripheral.discoverServices(nil)
        
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        print("""
                DID DISCONNECT
                (╯°□°）╯︵ ┻━┻
               """
        )
        
        NotificationCenter.default.post(name: Notification.Name("bottleDidDisconnect"), object: nil)
        keepScaning = true
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        guard let services = peripheral.services else {return}
        
        services.forEach{ service in
            
            if service.uuid == ScayaBottle.Services.iconServiceUUID.cbUUID() {
                print("GOT SCAYA SERVICES")
                peripheral.discoverCharacteristics(nil, for: service)
            }
            
            if service.uuid == ScayaBottle.Services.ledServiceUUID.cbUUID() || service.uuid == ScayaBottle.Services.slideServiceUUID.cbUUID(){
                print("GOT other board Services")
                peripheral.discoverCharacteristics(nil, for: service)
            }
            
            if service.uuid == ScayaBottle.Services.firmwareUpdateServiceUUID.cbUUID() {
                print("GOT firmware services")
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        if let error = error {
            print("ERROR DISCOVERING CHARACTERISTICS: \(error.localizedDescription)")
        }
        
        guard let characteristics = service.characteristics else { return }
        characteristics.forEach{ characteristic in
            
            let id = characteristic.uuid
            
            switch id{
           
            case _ where id == ScayaBottle.Id.ledUUID.cbUUID():
                ledCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
                
            case _ where id == ScayaBottle.Id.sliderUUID.cbUUID():
                sliderCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
               
            case _ where id == ScayaBottle.Id.generalUUID.cbUUID():
                generalCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)

            case _ where id == ScayaBottle.Id.timeUUID.cbUUID():
                timeCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
                
            case _ where id == ScayaBottle.Id.recomendedUUID.cbUUID():
                recomendedCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
                
            case _ where id == ScayaBottle.Id.valuesUUID.cbUUID():
                valuesCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
                
            case _ where id == ScayaBottle.Id.dataSetsUUID.cbUUID():
                dataSetsCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
                
            case _ where id == ScayaBottle.Id.indexUUID.cbUUID():
                dataSetIndexCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
                
            case _ where id == ScayaBottle.Id.intervalUUID.cbUUID():
                intervalCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
                
            case _ where id == ScayaBottle.Id.progressUUID.cbUUID():
                progressCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
           
            case _ where id == ScayaBottle.Id.consumedUUID.cbUUID():
                consumedCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
           
            case _ where id == ScayaBottle.Id.clearAllUUID.cbUUID():
                clearAllCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
                
            case _ where id == ScayaBottle.Id.factoryResetUUID.cbUUID():
                factoryResetCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
                
            case _ where id == ScayaBottle.Id.reminderUUID.cbUUID():
                reminderCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
          
            //FIRMWARE
                
            case _ where id == ScayaBottle.Id.fwDataUUID.cbUUID():
                fwDataCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
                
            case _ where id == ScayaBottle.Id.crcUUID.cbUUID():
                crcCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
                
            case _ where id == ScayaBottle.Id.updateCMDUUID.cbUUID():
                updateCMDCharacteristic = characteristic
                waterBottle?.setNotifyValue(true, for: characteristic)
                
            default:
                print("Unknown char: \(characteristic.uuid)")
            }
        }
        
        print("Characterstics discovered")

        delegate.bottleConnected()
    }
    
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        
        if characteristic.isNotifying{
            print("Notification STARTED on characteristic: \(characteristic.uuid)")
        }else{
         //   print("No notificaiton from - \(characteristic.uuid)")
           // centralManager.cancelPeripheralConnection(peripheral)
        }
        
    }
    
    
    //MARK: - Characteristic updates
   
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        guard let data = characteristic.value else {
            print("No bytes")
            return
        }
        
        let id = characteristic.uuid
        switch characteristic.uuid {
            
        //GENERAL
        case _ where id == ScayaBottle.Id.generalUUID.cbUUID():
            generalCharacteristicUpdate(data)
   
        //TIME
        case _ where id == ScayaBottle.Id.timeUUID.cbUUID():
            timeCharacteristicUpdate(data)
  
        //RECOMENDED
        case _ where id == ScayaBottle.Id.recomendedUUID.cbUUID():
            recommendedCharacteristicUpdate(data)
            
        //VALUES
        case _ where id == ScayaBottle.Id.valuesUUID.cbUUID():
            valuesCharacteristicUpdate(data)
            
        //DATA SETS
        case _ where id == ScayaBottle.Id.dataSetsUUID.cbUUID():
            dataSetsCharacteristicUpdate(data)
            
        //LED LIGHT
        case _ where id == ScayaBottle.Id.ledUUID.cbUUID():
            ledCharacteristicUpdate(data)
        
        //PROGRESS UPDATE
        case _ where id == ScayaBottle.Id.progressUUID.cbUUID():
            BottleManager.shared.updateDrinikngProgress()
            BottleManager.shared.read(type: .general)
            
        //CRC
        case _ where id == ScayaBottle.Id.crcUUID.cbUUID():
            readCRC(data)
            
        case _ where id == ScayaBottle.Id.updateCMDUUID.cbUUID():
            readCRC(data)
            
        default:
            print("Updated value for: \(id)")
            
        }
    }
    
    //MARK: -
    
    fileprivate func readCRC(_ data: Data) {
        print("CRC data")
        print(Array(data))
    }
    
    fileprivate func generalCharacteristicUpdate(_ data: Data) {
        print("GENERAL UPDATED")
        print(data)
        
        let general = ScayaBottleGeneralCharacteristics(data: data)
        
        print(general)
        print("Errors - \(general.bottleError)" )
    }

    fileprivate func timeCharacteristicUpdate(_ data: Data) {
    
        let timeValue: Int32 = data.withUnsafeBytes { $0.pointee }
    
        print(timeValue)
    }
    
    fileprivate func recommendedCharacteristicUpdate(_ data: Data) {
        let recommendedValue: UInt32 = data.withUnsafeBytes { $0.pointee }
        print(recommendedValue)
    }
    
    fileprivate func valuesCharacteristicUpdate(_ data: Data) {
        let dataSet = ScayaBottleValue(data: data)
        print(dataSet)
    }
    
    fileprivate func dataSetsCharacteristicUpdate(_ data: Data) {
        if let dataSets = Int16(data: data){
            numberOfDataSets = Int(dataSets)
        }
        print("Avaliable dataSets: \(numberOfDataSets)")
        
        if numberOfDataSets > 0 {
            delegate.gotNewDataSets()
        }
    }
    
    fileprivate func ledCharacteristicUpdate(_ data: Data){
        
        var recived: [UInt8] = []
        recived  = Array(data)
        
        let color = UIColor(red: CGFloat(recived[0]/255), green: CGFloat(recived[1]/255), blue: CGFloat(recived[2]/255), alpha: CGFloat(recived[3]/100))
        delegate.didReadColor(color: color)
    }
}

