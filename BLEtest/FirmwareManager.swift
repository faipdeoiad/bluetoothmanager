//
//  FirmwareManager.swift
//  BLEtest
//
//  Created by Žiga Porenta on 25/10/2017.
//  Copyright © 2017 umwerk. All rights reserved.
//

import Foundation

struct FirmwareUpdateData {
    
    let blockCount: UInt8
    //Number of data bolock within a line 0..15
    
    let arrayID: UInt8
    //ArrayID of the line
    
    let rowNumber: [UInt8]
    //Line number
    
    let data: [UInt8]
    //Block with the lenght of 16 byte of the firmware data

    static func initializationPacket() -> FirmwareUpdateData{
    
        let dataArray: [UInt8] = Array(repeating: UInt8(0xAA), count: 16)
        let initPacket = FirmwareUpdateData(blockCount: 0, arrayID: 254, rowNumber: [255,255], data: dataArray)
        return initPacket
        
    }
    
    var packetData: Data{
        
        var commandData = Data()
        var commandPacket = [UInt8]()
        
        commandPacket.append(blockCount)
        commandPacket.append(arrayID)
        
        rowNumber.forEach{
            commandPacket.append($0)
        }
    
        data.forEach{
            commandPacket.append($0)
        }
        
        commandData = Data(bytes: commandPacket)
  
        return commandData
    }
}

struct FirmwareUpdateManager {
    
    static var firmwareData: [Data] = [Data]()
    static var appCRC: CRC32?
    
    static let btManager = BluetoothManager.shared
    
    static func updateFirmware(){
        
        print("Will update firmware")
        
        print(Array(firmwareData[0]))
        print(firmwareData.count)
        
        
        firmwareData.forEach{
            
            writeFirmware(data: $0)
            let check = CRC32(data: $0)
            appCRC = check
         //   print(Array(check.crc.data))
            //let wot = check.crc
            
        }
        
        if let crc = appCRC{
            let crcArray = Array(crc.crc.data)
            print("-----APP CRC------")
            print(crcArray)
        }
        
        
    }
    
    private static func writeFirmware(data: Data){
        
       // print("Writing firmware chunk")
        
        if let fwDataCharacteristic = btManager.fwDataCharacteristic{
            btManager.waterBottle?.writeValue(data, for: fwDataCharacteristic, type: .withoutResponse)
        }else{
            print("Could not write fwData")
        }
    }
    
    static func parseData(){
        
        firmwareData.removeAll()
        
        //TEMP local resourse
        if let path = Bundle.main.path(forResource: "ICon_V0.4_Snr-A1B2C3D4E5F2", ofType: "cyacd"){
            
            do{
                
                //Set init packet
                firmwareData.append(FirmwareUpdateData.initializationPacket().packetData)
                
                let testString = try String(contentsOfFile: path, encoding: .utf8)
                
                var stringsArray = testString.components(separatedBy: ":")
                stringsArray.removeFirst()
                stringsArray.forEach{
                    let trimmedHexString = $0.trimmingCharacters(in: .whitespacesAndNewlines)
                    createFirmwareData(dataHexString: trimmedHexString)
                
                }
            
            }catch{
                print("COULD NOT PARSE STRING")
            }
        }
    }
    

    private static func createFirmwareData(dataHexString: String){
        
        guard let hexData = Data(hexString: dataHexString) else { return }
        var dataArray = Array(hexData)
        
        let arrayId = UInt8(dataArray[0])
        
        let rowData = Data(dataArray[1...2])
        let rowInt = Array(rowData)
    
        let lenghtData = Data(dataArray[3...4])
        let _: UInt16 = lenghtData.withUnsafeBytes { $0.pointee } //lenght
       // print(lenghtInt)
        
        let _ = Int(dataArray.last!) //checksum
       // print(checkSum)
        
        //Create the 8 20byte packages
        Array(0...7).forEach{
            
            var actualDataArray = [UInt8]()
            var startIndex = 5
            if $0 != 0{ startIndex += (16 * $0) }
            
            Array(0...15).forEach{
                actualDataArray.append(UInt8(dataArray[startIndex+$0]))
            }
            
            let fwData = FirmwareUpdateData(blockCount: UInt8($0), arrayID: arrayId, rowNumber: rowInt, data: actualDataArray)
            
           // print(fwData)
            let updateData = fwData.packetData
            firmwareData.append(updateData)
            
        }
    }
}
