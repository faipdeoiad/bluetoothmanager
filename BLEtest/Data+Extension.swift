//
//  Data+Extension.swift
//  SCAYA
//
//  Created by Žiga Porenta on 16/10/2017.
//  Copyright © 2017 umwerk. All rights reserved.
//

import Foundation

//MARK: - DataConvertable

protocol DataConvertible {
    
    init?(data: Data)
    var data: Data { get }
    
}

extension DataConvertible {
    
    init?(data: Data) {
        
        guard data.count == MemoryLayout<Self>.size else { return nil }
        self = data.withUnsafeBytes { $0.pointee }
        
    }
    
    var data: Data{
        var value = self
        return Data(buffer: UnsafeBufferPointer(start: &value, count: 1))
    }
    
}

extension Int : DataConvertible { }
extension Int8 : DataConvertible { }
extension Int16 : DataConvertible { }
extension Int32 : DataConvertible { }
extension UInt8 : DataConvertible { }
extension UInt16 : DataConvertible { }
extension UInt32 : DataConvertible { }

extension String : DataConvertible {
    
    init?(data: Data) {
        self.init(data: data, encoding: .utf8)
    }
    var data: Data {
        return self.data(using: .utf8)!
    }
    
    subscript (r: CountableClosedRange<Int>) -> String {
        let startIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let endIndex = self.index(startIndex, offsetBy: r.upperBound - r.lowerBound)
        return String(self[startIndex...endIndex])
    }
    
    subscript (bounds: CountableRange<Int>) -> String {
        let startIndex =  self.index(self.startIndex, offsetBy: bounds.lowerBound)
        let endIndex = self.index(startIndex, offsetBy: bounds.upperBound - bounds.lowerBound)
        return String(self[startIndex..<endIndex])
    }
}


//MARK: - Base data extensions

extension Data{
    
    func hexEncodedString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
    
    init?(hexString: String) {
        let len = hexString.count / 2
        var data = Data(capacity: len)
        for i in 0..<len {
            let j = i*2
            let bytes = hexString[j..<j+2]
            if var num = UInt8(bytes, radix: 16) {
                data.append(&num, count: 1)
            } else {
                return nil
            }
        }
        self = data
    }
    
    init<T>(fromArray values: [T]) {
        var values = values
        self.init(buffer: UnsafeBufferPointer(start: &values, count: values.count))
    }
    
    func toArray<T>(type: T.Type) -> [T] {
        return self.withUnsafeBytes {
            [T](UnsafeBufferPointer(start: $0, count: self.count/MemoryLayout<T>.stride))
        }
    }
}
