//
//  ViewController.swift
//  BLEtest
//
//  Created by Žiga Porenta on 25/09/2017.
//  Copyright © 2017 umwerk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var readColorButton: UIButton!
    @IBOutlet weak var connectionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        BottleManager.shared.connectToBottle()
        NotificationCenter.default.addObserver(self, selector: #selector(bottleConnected), name: Notification.Name("bottleDidConnect"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(bottleDisconnected), name: Notification.Name("bottleDidDisconnect"), object: nil)
        
        ///
        //FirmwareUpdateManager.parseData()
    
    }
    
    
    @IBAction func readColor(_ sender: UIButton) {
        
      //  BottleManager.shared.read(type: .crc)
       // BottleManager.shared.read(type: .cmdUpdate)

        
       // print(FirmwareUpdateData.initializationPacket().packetData)
        
        print("BOTTLE CRC")
        BottleManager.shared.read(type: .crc)
        BottleManager.shared.read(type: .cmdUpdate)

        /*
        print("Read value")
        BottleManager.shared.read(type: .fwData)
        BottleManager.shared.read(type: .cmdUpdate)
         */
    }
    
    @IBAction func writeColor(_ sender: Any) {
        
        
        FirmwareUpdateManager.parseData()
        FirmwareUpdateManager.updateFirmware()
        
 
        
        //  BottleManager.shared.writeColor()
        
    }
    
    //MARK: - Conection
    
    @objc func bottleConnected(){
        connectionLabel.text = "Connected"
    }
    
    @objc func bottleDisconnected(){
        connectionLabel.text = "Not connected"
    }
}
