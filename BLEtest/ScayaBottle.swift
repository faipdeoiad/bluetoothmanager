//
//  ScayaBottle.swift
//  SCAYA
//
//  Created by Žiga Porenta on 27/09/2017.
//  Copyright © 2017 umwerk. All rights reserved.
//

import Foundation
import CoreBluetooth

enum ScayaBottle: String{
    
    case name = "ICon"
    case restorationId = "com.scaya.BLEConnect.CentralManager"
    
    enum Id: String{
        
        case runTimeUUID    = "00002902-0000-1000-8000-00805F9B34FB"
        case ledUUID        = "0003CBB1-0000-1000-8000-00805F9B0131"
        case sliderUUID     = "0003CAA2-0000-1000-8000-00805F9B0131"
        
        //Read only
        case generalUUID    = "D6988BB4-3B1E-44F0-BABA-21A60CE74947"
        
        // Seconds since 1.1.70
        // R/W
        case timeUUID       = "CD9E93E4-80A3-4832-9736-63682929394E"
        
        // Tells the bottle to show a reminder at the designated time
        // W
        case reminderUUID   = "C8DF0891-A01C-4E6F-8AAB-7316B04A307B"
        
        // Interval when to show an automatic reminder when no new reminder has been set in the meantime.
        // W
        case intervalUUID   = "95590973-E800-4885-9FB4-E54E42E739D0"
        
        // Notify for sending a new dataset to bottle.
        // R
        case progressUUID   = "51CF866C-98A1-4AE9-90D0-EC08D62242A5"
        
        // Percent
        // W
        case recomendedUUID = "34D00FA1-A59D-4B38-857C-D8FFDF0D1B1D"
        case consumedUUID   = "441D8CBD-3AE7-4CCE-B719-6285EDB520C7"
        
        
        // Number of available datasets in the drinklog
        // R
        case dataSetsUUID   = "A6766F4D-DC58-4016-95BC-2855157836FE"
        
        // Index of the dataset which should be transmitted to App
        // W
        case indexUUID      = "4A4F5D16-677C-4CB3-B316-297022A93D1D"
        
        //Oldest entry from list of saved data, will be automatically removed from list.
        //Time is uint32_t. complete struct has length of 6 byte
        case valuesUUID     = "1A11EC9A-BA01-4AD9-A6D5-A7D87B3CE090"
        
        //1: Clear all values from list, 0: do nothing
        case clearAllUUID   = "072D3796-8807-4551-854E-231FE5242F1F"
        
        //1: Factory reset, 0: do nothing - draft
        case factoryResetUUID = "593B564A-FAD5-4744-9817-4E0034989B68"
        
        
        ///Firmware updates
        //Read and write
        case fwDataUUID = "9f53a0a0-5864-11e5-b5db-0002a5d5c51b"
        
        //Read
        case crcUUID = "296eb640-5869-11e5-9c12-0002a5d5c51b"
    
        //Read and write
        case updateCMDUUID = "327944c0-587e-11e5-b635-0002a5d5c51b"
        
        
        func cbUUID() -> CBUUID{
            return CBUUID(string: self.rawValue)
        }
    }
    
    enum Services: String{
        
        case iconServiceUUID           = "AB642D74-1EA9-4BF6-98E9-8A3059BF546A"
        case firmwareUpdateServiceUUID = "4fc73f60-5864-11e5-911d-0002a5d5c51b"
        
        case ledServiceUUID            = "0003CBBB-0000-1000-8000-00805F9B0131"
        case slideServiceUUID          = "0003CAB5-0000-1000-8000-00805F9B0131"
        
        
        
        func cbUUID() -> CBUUID{
            return CBUUID(string: self.rawValue)
        }
    }
}

//MARK: - Bottle values

struct ScayaBottleValue {
    
    let level: Int?
    let temp: Int?
    let time: Int?
    
    init(data: Data) {
        
        let dataArray  = Array(data)
        let timeData = Data(dataArray[0...3])
        
        //TIME:
        if let timeU32 = Int32(data: timeData){
            time = Int(timeU32)
        }else{
            time = nil
        }
        
        level = Int(dataArray[4])
        temp =  Int(dataArray[5])
        
    }
}

//MARK: - Bottle general

struct ScayaBottleGeneralCharacteristics {
    
    let lenght: Int?
    let version: Int?
    let model: String?
    let serialNumber: String?
    let firmWareMajor: Int?
    let firmWareMinor: Int?
    let temperature: Int?
    let battery: Int?
    let level: Int?
    

    //Errors
    enum BottleError{
        case levelSensor
        case temperatureSensor
        case battery
        case undefinedError
    }
    
    var bottleErrors = [BottleError]()
    var bottleError: Bool{
        return !bottleErrors.isEmpty
    }
    ///////
    
    init(data: Data) {
        
        let dataArray  = Array(data)
       
        lenght = Int(dataArray[0])
        version = Int(dataArray[1])
        
        let modelData = Data(dataArray[2...5])
        model = String(data: modelData)
        
        //Serial
        let serialArray = dataArray[6...11]
        let serialNumberData = Data(serialArray)
        serialNumber = serialNumberData.hexEncodedString()
        
        firmWareMajor = Int(dataArray[12])
        firmWareMinor = Int(dataArray[13])
    
        temperature = Int(dataArray[14])
        battery = Int(dataArray[15])
   
        level = Int(dataArray[16])
        
        //Status
        bottleErrors.removeAll()
        let statusData = Data(dataArray[17...18])
        let statusInt: Int16 = statusData.withUnsafeBytes { $0.pointee }
        let statusInts = String(statusInt, radix: 2).map{ Int(String($0)) }
 
        //LEVEL
        if statusInts.count > 0, let bit = statusInts[0]{
            if bit == 1 {
                bottleErrors.append(.levelSensor)
            }
        }
        
        //TEMPERATURE
        if statusInts.count > 1, let bit = statusInts[1]{
            if bit == 1 {
                bottleErrors.append(.temperatureSensor)
            }
        }
        
        //BATTERY
        if statusInts.count > 2, let bit = statusInts[2]{
            if bit == 1 {
                bottleErrors.append(.battery)
            }
        }
        
        //UNDEFINED
        if statusInts.count > 15, let bit = statusInts[15]{
            if bit == 1 {
                bottleErrors.append(.undefinedError)
            }
        }
    }
}

