//
//  BottleManager.swift
//  SCAYA
//
//  Created by Žiga Porenta on 17/10/2017.
//  Copyright © 2017 umwerk. All rights reserved.
//

import UIKit
import CoreBluetooth

struct BottleManager{
    
    static let shared = BottleManager()
    let btManager = BluetoothManager.shared
    
    
    enum BottleReadType {
        case general, time, progress, sets, fwData, crc, cmdUpdate
    }
    
    func connectToBottle(){

        btManager.delegate = self
     
        guard let bottle = btManager.waterBottle else {
            scanForBottle()
            return
        }
        
        if bottle.state != .connected{
            scanForBottle()
        }else{
            print("Bottle is already connected")
        }
    }
    
    private func scanForBottle(){
        if btManager.keepScaning{
            btManager.scanForDevices()
        }
    }
    
    //MARK: - Action/Write

     func updateDrinikngProgress(){
        print("Updating waterprogress")
        
        //TODO: get real numbers from dashboard
        let recommendedValue: UInt8 = 80
        let consumedValue: UInt8 = 20
        
        let recomendedData = recommendedValue.data
        let consumedData = consumedValue.data
    
        if let recomededChar = btManager.recomendedCharacteristic, let consumedChar = btManager.consumedCharacteristic{
            btManager.waterBottle?.writeValue(recomendedData, for: recomededChar, type: .withResponse)
            btManager.waterBottle?.writeValue(consumedData, for: consumedChar, type: .withResponse)
        }else{
            print("Could not write progress data")
        }
    }
    
    
     func writeSecondsFrom1970(){
        print("Writing seconds")
        
        let sec: Int32 = Int32(Date().timeIntervalSince1970)
        let secondsData = sec.data
        
        if let recommendedChar = btManager.timeCharacteristic{
            btManager.waterBottle?.writeValue(secondsData, for: recommendedChar, type: .withResponse)
        }else{
            print("Could not write time")
        }
    }
    
    func clearAllValues(){
        
        let clearValue: UInt8 = 1
        let clearData = clearValue.data
        
        if let clearAllChar = btManager.clearAllCharacteristic{
            btManager.waterBottle?.writeValue(clearData, for: clearAllChar, type: .withResponse)
        }else{
            print("Could not clear all values")
        }
    }
    
     func writeColor(){
        print("Writing color")
        let colorValue:[UInt8] = [0,255,78,100]
        let data = Data(colorValue)
        
        if let ledChar = btManager.ledCharacteristic{
            btManager.waterBottle?.writeValue(data, for: ledChar, type: .withResponse)
        }else{
            print("No ledCharacteristic")
        }
    }
    
     func writeDataSetIndex(index: Int){
        
        print("Settng data index: \(index)")
        
        let data = Int16(index).data
        
        if let char = btManager.dataSetIndexCharacteristic{
            btManager.waterBottle?.writeValue(data, for: char, type: .withResponse)
            
            if let values = btManager.valuesCharacteristic{
                btManager.waterBottle?.readValue(for: values)
            }
            
        }else{
            print("Could not set data index")
        }
    }
    
    //MARK: - Read
    
    func read(type: BottleReadType){
        
        var btChar: CBCharacteristic?
        
        switch type {
        case .general:
            btChar = btManager.generalCharacteristic
        case .time:
            btChar = btManager.timeCharacteristic
        case .progress:
            btChar = btManager.progressCharacteristic
        case .sets:
             btChar = btManager.dataSetsCharacteristic
        case .cmdUpdate:
            btChar = btManager.updateCMDCharacteristic
        case .crc:
            btChar = btManager.crcCharacteristic
        case .fwData:
            btChar = btManager.fwDataCharacteristic
           
        }
        
        if let btChar = btChar{
            btManager.waterBottle?.readValue(for: btChar)
        }
    }
    
    //MARK: - Firmware update
    
}

//MARK: - Bottle delegate

extension BottleManager: WaterBottleDelegate{
 
     func bottleConnected() {
        
        NotificationCenter.default.post(name: Notification.Name("bottleDidConnect"), object: nil)
        BottleManager.shared.writeSecondsFrom1970()
        read(type: .general)
        read(type: .sets)

    }
    
    func didReadColor(color: UIColor) {
        print("Read color")
    }
    
    func gotNewDataSets() {
        Array(0...btManager.numberOfDataSets - 1).forEach{
            writeDataSetIndex(index: $0)
        }
    }
}
